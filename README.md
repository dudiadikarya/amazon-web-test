#  WebUI Automation Testing with Cucumber & Serenity

---


### Note From Author:
Dear, Mekari Recruitment Team and kak Edo! 

I indeed have read the 'How to' file on working this Test, and I've noticed that you require to work on cucumber-ruby. However, in this short time, I realized that I don't have the capabilities to learn a complete new programming language in such a short time, I've tried to learn and work on ruby however but I got stuck and decided to use the programming language that I'm most comfortable with. If maybe you want to check the repo of me trying to learn cucumber-ruby I've added kak Edo as a collaborator there, and here's the repo link: https://bitbucket.org/dudiadikarya/booker-ruby-cucumber

I decided to use **Cucumber Java** for this WebUI Automation. I used my own boilerplate that I created in github Repo, kindly check it if you want: https://github.com/dudiadikaryaa/cucumber-serenity

I'd appreciate it so much if you still want to check my work, but if it **has to use** cucumber-ruby feel free to ignore this work!

Thank you for your time and attention!

Regards,

Dudi Adikarya

---

![Cucumber Logo](./docs/img/cucumber-logo.png)

![Serenity Logo](./docs/img/serenity-logo.png)

> Cucumber is a testing tool that supports Behavior Driven Development (BDD). It offers a way to write tests that anybody can understand, regardless of their technical knowledge. https://cucumber.io/

> Serenity BDD is a simple but powerful way to produce illustrated, narrative Test Reports that document and describe what your application does and how it works. www.thucydides.info/

### Prerequisites:

- Java 1.8
- Maven 3.5.2 . How to Install on Windows/Linux/Mac please read [here](https://www.baeldung.com/install-maven-on-windows-linux-mac).
- IntelliJ IDEA or other Text Editor.
- Chromedriver 81.0 (Depends on your Chrome Version)

### Get Started:
Clone this project repository into your local:
```sh
$ git clone https://dudiadikarya@bitbucket.org/dudiadikarya/amazon-web-test.git
```

After you have successfully cloned this project repository, do the following:

```sh
$ cd amazon-web-test
$ mvn install
```

### How to Run:

```sh
$ mvn clean verify                                          | Run all test & generate Test Report
$ mvn clean verify -Dcucumber.options="--tags @login"       | Run only @login tag & generate Test Report
```

### How does it work?
For the detail explanation, please go [here](docs/details.md) !

### Reference
- https://howtodoinjava.com/maven/how-to-install-maven-on-windows/
- https://serenity-bdd.github.io/theserenitybook/latest/cucumber.html
- https://www.guru99.com/introduction-to-cucumber.html
@register
Feature: Register Scenarios
  As a user
  I want to register to amazon system
  So that I have an authorized account

  @positive_1
  Scenario: Successfully Redirected to Register Page
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    Then the user redirects to Register Page

  ##NOTE: Change the Email Address First before running the register feature
  @positive_2
  Scenario: Successfully Register
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input IlhamDudi in name field
    And the user input duditest0000@gmail.com in email field
    And the user input testing123 in password field
    And the user input testing123 in confirm password field
    And the user clicks Submit Register button
    Then the user redirects to OTP Page

  @negative_1
  Scenario: Failed to register due to blank name fields
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input ilhamdudi16@gmail.com in email field
    And the user input testing123 in password field
    And the user input testing123 in confirm password field
    And the user clicks Submit Register button
    Then show error message name

  @negative_2
  Scenario: Failed to register due to blank email fields
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input IlhamDudi in name field
    And the user input testing123 in password field
    And the user input testing123 in confirm password field
    And the user clicks Submit Register button
    Then show error message email

  @negative_3
  Scenario: Failed to register due to blank password fields
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input IlhamDudi in name field
    And the user input ilhamdudi16@gmail.com in email field
    And the user clicks Submit Register button
    Then show error message password

  @negative_4
  Scenario: Failed to register due to blank confirm password fields
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input IlhamDudi in name field
    And the user input ilhamdudi16@gmail.com in email field
    And the user input testing123 in password field
    And the user clicks Submit Register button
    Then show error message confirmPassword

  @negative_5
  Scenario: Failed to register due to different password and confirm password fields
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input IlhamDudi in name field
    And the user input ilhamdudi16@gmail.com in email field
    And the user input testing1234 in password field
    And the user input testing1 in confirm password field
    And the user clicks Submit Register button
    Then show error message unmatchPassword

  @negative_6
  Scenario: Failed to register due to registered email
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input IlhamDudi in name field
    And the user input reiji.morie@gmail.com in email field
    And the user input testing123 in password field
    And the user input testing123 in confirm password field
    And the user clicks Submit Register button
    Then show error message registeredEmail

  @negative_7
  Scenario: Failed to register due to incorrect format email
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input IlhamDudi in name field
    And the user input reiji.morie in email field
    And the user input testing123 in password field
    And the user input testing123 in confirm password field
    And the user clicks Submit Register button
    Then show error message incorrectEmail

  @negative_8
  Scenario: Failed to register due to incorrect format password
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user clicks Register button
    And the user input IlhamDudi in name field
    And the user input reiji.morie@gmail.com in email field
    And the user input test in password field
    And the user input test in confirm password field
    And the user clicks Submit Register button
    Then show error message incorrectPassword
@login
Feature: Login Scenarios
  As a user
  I want to log in into the amazon system
  So that I'm authorized to make transactions

  @negative_1
  Scenario: Login failed using incorrect email
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user input qwer@qwer in credentials login field
    And the user click Continue button
    Then show login error message email

  @negative_2
  Scenario: Login failed using incorrect phone number
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user input 1234 in credentials login field
    And the user click Continue button
    Then show login error message phone

  @negative_3
  Scenario: Login failed using incorrect password
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user input reiji.morie@gmail.com in credentials login field
    And the user click Continue button
    And the user input asdf123 in password login field
    And the user click Submit Login button
    Then show login error message password

  @positive_1
  Scenario: Successfully login with correct email & password
    Given the user opens Amazon Web
    When the user clicks Sign In button
    And the user input correct in credentials login field
    And the user click Continue button
    And the user input correct in password login field
    And the user click Submit Login button
    Then the user logged in
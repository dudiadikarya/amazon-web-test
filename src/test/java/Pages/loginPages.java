package Pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import Helper.credentials;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class loginPages extends PageObject{

    credentials credentials;

    //Input Credentials
    @FindBy(id = "ap_email")
    WebElement credentialsField;

    @FindBy(id = "continue")
    WebElement continueButton;

    public void inputCredentials(String credential){
        if (credential.equals("correct")){
            String email = credentials.user_email;
            waitForCondition().until(ExpectedConditions.elementToBeClickable(continueButton));

            element(credentialsField).shouldBeVisible();
            credentialsField.sendKeys(email);
        }else{
            waitForCondition().until(ExpectedConditions.elementToBeClickable(continueButton));

            element(credentialsField).shouldBeVisible();
            credentialsField.sendKeys(credential);
        }
    }

    //Click Continue
    public void clickContinue(){
        element(continueButton).shouldBeVisible();
        continueButton.click();
    }

    //Input Password
    @FindBy(id = "ap_password")
    WebElement passwordField;

    @FindBy(id = "signInSubmit")
    WebElement signInSubmit;

    public void inputPassword(String password){
        if (password.equals("correct")){
            String correctPassword = credentials.user_password;
            waitForCondition().until(ExpectedConditions.elementToBeClickable(signInSubmit));

            element(passwordField).shouldBeVisible();
            passwordField.sendKeys(correctPassword);
        }else{
            waitForCondition().until(ExpectedConditions.elementToBeClickable(signInSubmit));
            element(passwordField).shouldBeVisible();
            passwordField.sendKeys(password);
        }
    }

    //Click Submit Login
    public void clickSubmitLogin(){
        element(signInSubmit).shouldBeVisible();
        signInSubmit.click();
    }

    //Assert Error Messages
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")
    WebElement emailMessage;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")
    WebElement passwordError;

    public void assertLoginError(String errorType){
        if (errorType.equals("email")){
            element(emailMessage).shouldBeVisible();
            String emailErrorText = emailMessage.getText();

            assertEquals("Failure - strings are not equal", "We cannot find an account with that email address", emailErrorText);
        }else if(errorType.equals("phone")){
            element(emailMessage).shouldBeVisible();
            String phoneErrorText = emailMessage.getText();

            assertEquals("Failure - strings are not equal", "We cannot find an account with that mobile number", phoneErrorText);
        }else if(errorType.equals("password")){
            element(passwordError).shouldBeVisible();
            String passwordErrorText = passwordError.getText();

            assertEquals("Failure - strings are not equal", "Your password is incorrect", passwordErrorText);
        }
    }

    //Assert Logged In
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div/div/div[1]/div/div/form/div[1]/h1")
    WebElement authenticationPage;

    public void assertLoggedIn(){
        //Amazon prevent login from automation tools, so I will assert on the Authentication Page
        element(authenticationPage).shouldBeVisible();
        String authenticationPageText = authenticationPage.getText();

        assertEquals("Failure - strings are not equal", "Authentication required", authenticationPageText);
    }

}

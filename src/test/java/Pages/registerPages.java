package Pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertEquals;

public class registerPages extends PageObject{

    //Click Sign In Button
    @FindBy(id = "nav-link-accountList")
    WebElement signInButton;

    public void clickSignInPage(){
        waitForCondition().until(ExpectedConditions.elementToBeClickable(signInButton));
        signInButton.click();
    }

    //Click Register Button
    @FindBy(id = "createAccountSubmit")
    WebElement registerButton;

    public void clickRegisterButton(){
        waitForCondition().until(ExpectedConditions.elementToBeClickable(registerButton));
        registerButton.click();
    }

    //Assert on Register Page
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/h1")
    WebElement registerTitle;

    public void assertOnRegisterPage(){
        element(registerTitle).shouldBeVisible();
        String titleText = registerTitle.getText();

        assertEquals("Failure - strings are not equal", "Create account", titleText);
    }

    //Input Name
    @FindBy(id = "ap_customer_name")
    WebElement nameField;

    public void inputName(String userName){
        element(nameField).shouldBeVisible();
        nameField.sendKeys(userName);
    }

    //Input Email
    @FindBy(id = "ap_email")
    WebElement emailField;

    public void inputEmail(String userEmail){
        element(emailField).shouldBeVisible();
        emailField.sendKeys(userEmail);
    }

    //Input Password
    @FindBy(id = "ap_password")
    WebElement passwordField;

    public void inputPassword(String password){
        element(passwordField).shouldBeVisible();
        passwordField.sendKeys(password);
    }

    //Input Confirm Password
    @FindBy(id = "ap_password_check")
    WebElement confirmPasswordField;

    public void inputConfirmPassword(String password){
        element(confirmPasswordField).shouldBeVisible();
        confirmPasswordField.sendKeys(password);
    }

    //Click Submit Register
    @FindBy(id = "continue")
    WebElement submitButton;

    public void clickSubmitRegister(){
        waitForCondition().until(ExpectedConditions.elementToBeClickable(submitButton));
        submitButton.click();
    }

    //Assert Error Messages
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[1]/div/div/div")
    WebElement nameError;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[2]/div/div[1]/div/div")
    WebElement emailError;

    @FindBy(xpath = "//*[@id=\"auth-email-invalid-email-alert\"]/div/div")
    WebElement incorrectEmailError;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[3]/div[1]/div[2]/div/div")
    WebElement passwordError;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[3]/div[2]/div[1]/div/div")
    WebElement confirmPasswordError;

    @FindBy(xpath = "//*[@id=\"auth-password-invalid-password-alert\"]/div/div")
    WebElement incorrectPasswordError;

    @FindBy(xpath = "//*[@id=\"auth-password-mismatch-alert\"]/div/div")
    WebElement unmatchPasswordError;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/div/div/h4")
    WebElement registeredEmailPage;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/div[1]/a")
    WebElement registeredButtonAssert;

    public void assertErrorMessageRegis(String errorType){
        if (errorType.equals("name")){
            element(nameError).shouldBeVisible();
            String nameText = nameError.getText();

            assertEquals("Failure - strings are not equal", "Enter your name", nameText);
        }else if(errorType.equals("email")){
            element(emailError).shouldBeVisible();
            String emailText = emailError.getText();

            assertEquals("Failure - strings are not equal", "Enter your email", emailText);
        }else if(errorType.equals("password")){
            element(passwordError).shouldBeVisible();
            String passwordText = passwordError.getText();

            assertEquals("Failure - strings are not equal", "Enter your password", passwordText);
        }else if(errorType.equals("confirmPassword")){
            element(confirmPasswordError).shouldBeVisible();
            String confirmPasswordText = confirmPasswordError.getText();

            assertEquals("Failure - strings are not equal", "Type your password again", confirmPasswordText);
        }else if(errorType.equals("unmatchPassword")){
            element(unmatchPasswordError).shouldBeVisible();
            String unmatchText = unmatchPasswordError.getText();

            assertEquals("Failure - strings are not equal", "Passwords must match", unmatchText);
        }else if(errorType.equals("registeredEmail")){
            waitForCondition().until(ExpectedConditions.elementToBeClickable(registeredButtonAssert));

            element(registeredEmailPage).shouldBeVisible();
            String registeredEmailText = registeredEmailPage.getText();

            assertEquals("Failure - strings are not equal", "Email address already in use", registeredEmailText);
        }else if(errorType.equals("incorrectEmail")){
            element(incorrectEmailError).shouldBeVisible();
            String incorrectEmailText = incorrectEmailError.getText();

            assertEquals("Failure - strings are not equal", "Enter a valid email address", incorrectEmailText);
        }else if(errorType.equals("incorrectPassword")){
            element(incorrectPasswordError).shouldBeVisible();
            String incorrectPasswordText = incorrectPasswordError.getText();

            assertEquals("Failure - strings are not equal", "Passwords must be at least 6 characters.", incorrectPasswordText);
        }
    }

    //Assert on OTP Page
    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/form/div[1]/div[1]/h1")
    WebElement verifyEmailTitle;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/form/div[4]/span/span/input")
    WebElement submitOTP;
    public void assertOnOTP(){
        waitForCondition().until(ExpectedConditions.elementToBeClickable(submitOTP));

        element(verifyEmailTitle).shouldBeVisible();
        String verifyText = verifyEmailTitle.getText();

        assertEquals("Failure - strings are not equal", "Verify email address", verifyText);
    }
}

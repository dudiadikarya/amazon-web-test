package Test;

import net.thucydides.core.annotations.Steps;
import Steps.registerSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

public class register {
    @Steps
    registerSteps registerSteps;

    @Given("the user opens Amazon Web")
    public void userOpenAmazonWeb(){
        registerSteps.openAmazonWeb();
    }

    @When("the user clicks Sign In button")
    public void userClickSignInButton(){
        registerSteps.clickSignInPage();
    }

    @And("the user clicks Register button")
    public void userClickRegisterButton(){
        registerSteps.clickRegisterButton();
    }

    @And("the user input {word} in name field")
    public void userInputName(String userName){
        registerSteps.inputName(userName);
    }

    @And("the user input {word} in email field")
    public void userInputEmail(String userEmail){
        registerSteps.inputEmail(userEmail);
    }

    @And("the user input {word} in password field")
    public void userInputPassword(String password){
        registerSteps.inputPassword(password);
    }

    @And("the user input {word} in confirm password field")
    public void userInputConfirmPassword(String confirmPassword){
        registerSteps.inputConfirmPassword(confirmPassword);
    }

    @And("the user clicks Submit Register button")
    public void userClickSubmitRegister(){
        registerSteps.clickSubmitRegister();
    }

    @Then("show error message {word}")
    public void showErrorMessageRegister(String errorType){
        registerSteps.assertErrorMessageRegis(errorType);
    }

    @Then("the user redirects to Register Page")
    public void userRedirRegisterPage(){
        registerSteps.assertOnRegisterPage();
    }

    @Then("the user redirects to OTP Page")
    public void userRedirOTPPage(){
        registerSteps.assertOnOTP();
    }
}

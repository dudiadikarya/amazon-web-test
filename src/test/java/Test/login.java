package Test;

import net.thucydides.core.annotations.Steps;
import Steps.loginSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class login {
    @Steps
    loginSteps loginSteps;

    @And("the user input {word} in credentials login field")
    public void userInputCred(String credential){
        loginSteps.inputCredentials(credential);
    }

    @And("the user click Continue button")
    public void userClickContinue(){
        loginSteps.clickContinue();
    }

    @And("the user input {word} in password login field")
    public void userInputPassword(String password){
        loginSteps.inputPassword(password);
    }

    @And("the user click Submit Login button")
    public void userClickSubmitLogin(){
        loginSteps.clickSubmitLogin();
    }

    @Then("show login error message {word}")
    public void showErrorMessageLogin(String errorType){
        loginSteps.assertLoginError(errorType);
    }

    @Then("the user logged in")
    public void userLoggedIn(){
        loginSteps.assertLoggedIn();
    }
}

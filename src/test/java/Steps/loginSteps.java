package Steps;

import Pages.loginPages;
import net.thucydides.core.annotations.Step;

public class loginSteps {
    loginPages loginPages;

    //Input Credentials Phone/Email
    @Step
    public void inputCredentials(String credential){
        loginPages.inputCredentials(credential);
    }

    //Input Password
    @Step
    public void inputPassword(String password){
        loginPages.inputPassword(password);
    }

    //User Click Continue
    @Step
    public void clickContinue(){
        loginPages.clickContinue();
    }

    //Click Submit Login
    @Step
    public void clickSubmitLogin(){
        loginPages.clickSubmitLogin();
    }

    //Assert login error
    @Step
    public void assertLoginError(String errorType){
        loginPages.assertLoginError(errorType);
    }

    //Assert User Logged In
    @Step
    public void assertLoggedIn(){
        loginPages.assertLoggedIn();
    }

}

package Steps;

import Pages.registerPages;
import net.thucydides.core.annotations.Step;

public class registerSteps {
    registerPages registerPages;

    //Open Amazon Web
    @Step
    public void openAmazonWeb(){
        registerPages.open();
    }

    //Click Sign In
    @Step
    public void clickSignInPage(){
        registerPages.clickSignInPage();
    }

    //Click Register Button
    @Step
    public void clickRegisterButton(){
        registerPages.clickRegisterButton();
    }

    //On Register Page
    @Step
    public void assertOnRegisterPage(){
        registerPages.assertOnRegisterPage();
    }

    //Input Name
    @Step
    public void inputName(String userName){
        registerPages.inputName(userName);
    }

    //Input Email
    @Step
    public void inputEmail(String userEmail){
        registerPages.inputEmail(userEmail);
    }

    //Input Password
    @Step
    public void inputPassword(String password){
        registerPages.inputPassword(password);
    }

    //Input Confirm Password
    @Step
    public void inputConfirmPassword(String confirmPassword){
        registerPages.inputConfirmPassword(confirmPassword);
    }

    //Click Submit Register
    @Step
    public void clickSubmitRegister(){
        registerPages.clickSubmitRegister();
    }

    //Assert Error Message
    @Step
    public void assertErrorMessageRegis(String errorType){
        registerPages.assertErrorMessageRegis(errorType);
    }

    //Assert OTP Page
    @Step
    public void assertOnOTP(){
        registerPages.assertOnOTP();
    }
}
